/*
1.5 �������� ��������� ��������� ������ ���������� �������.
*/


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{

	char buf[80];
	fgets(buf, 80, stdin);
	chomp(buf); //������� ������� ������


	//����� �� 40+��� ����� ������, ����������������� �� ������� ����
	fprintf(stdout, "%*s\n", 40+strlen(buf) / 2, buf);


	return 0;
}