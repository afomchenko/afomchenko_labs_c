#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main()
{
	float in;
	int i=1;
	char a;

	do
	{
		printf("input angle 000.00R or 000.00D:  ");
		scanf("%f%c", &in, &a);
		if (a == 'D' || a == 'd' || a == 'R' || a == 'r') i=2;
		else printf("unknown angle type\n");
		fflush(stdin);
	} while (i == 1);

	


	if (a == 'D' || a == 'd')
		{
		while (in > 360){ in -= 360.f; }
		printf("\n%3.3f%c = %3.3lf rad\n", in, 0xF8, (in / 360.f)*3.1415f);
		}
	else
		{
		while (in > 3.14159){ in -= 3.14159f; }
		printf("\n%3.3lf rad = %3.3lf%c\n", in, ((in / 3.14159f) * 360.f), 0xF8);
		}

	return 0;
}