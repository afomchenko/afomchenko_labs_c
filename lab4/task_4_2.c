/*
4.2 �������� ���������, ������� ��������� ������������ ������
��������� ����� � ����������, � ����� ��������� �� � �������
����������� ����� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{
	char buf[256];
	char *p[256];

	unsigned int exit = 0, i, j = 0, k;

	fgets(buf, 256, stdin);
	chomp(buf);

	for (i = 0; i < strlen(buf); i++)
	{
		if (!isalnum(buf[i]) && isalnum(buf[i + 1])) p[j] = &buf[i+1], j++;
		else if (isalnum(buf[i]) && i == 0) p[j] = &buf[i], j++;
		else if (isalnum(buf[i]) && (!isalnum(buf[i + 1])|| buf[i] == 0)) p[j] = &buf[i], j++;
	}

	//�����
	for (k = j-2; k >= 0; k -= 2){
		for (i = 0; p[k] + i <= p[k+1]; i++)
			printf("%c", *(p[k] + i));
		if (k == 0) break;
		putchar(' ');
	}
	putchar('\n');

	return 0;
}