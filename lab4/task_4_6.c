/*
4.3 �������� ���������, ������� ����������� ������ � ��������, �������� �� ������ �����������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{
	char name[256][256];
	unsigned int age[256];
	char *old, *young;
	unsigned int n, i, oldest=0, youngest=4000000000;

	printf("Number of people:   ");
	scanf("%d", &n);

	for (i = 1; i <= n; i++){
		fflush(stdin);
		printf("Name:   ");
		fgets(name[i], 256, stdin);
		chomp(name[i]);
		printf("Age:   ");
		scanf("%d", &age[i]);
		if (age[i] > oldest) old = &name[i][0], oldest = age[i];
		if (age[i] < youngest) young = &name[i][0], youngest = age[i];
	}

	puts("----------");
		
		printf("youngest:   %s\n", young);
		printf("oldest:   %s\n", old);

	return 0;
}