/*
4.3 �������� ���������, ������� ����������� ������ � ��������, �������� �� ������ �����������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{
	char buf[256];
	char *p=buf;

	unsigned int i, j, palindrom = 1;

	fgets(buf, 256, stdin);
	chomp(buf);

	for (i = 0, j=strlen(buf)-1; i < j; i++, j--)
	{
		if (*(p + j) != *(p + i)) {palindrom = 0; break;}
	}

	if (palindrom == 0) puts("not palindrom");
	else puts("palindrom!");

	return 0;
}