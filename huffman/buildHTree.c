﻿#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#define SIZE 256
#define CHAR_VARS 256
extern int fsz;


CNODE * getNode(CHARC charc)
{
	CNODE *temp = (CNODE*)malloc(sizeof(CNODE));
	temp->weight = (double)charc.count/fsz;
	temp->ch = (unsigned int)charc.ch;
	temp->code[0] = 0;
	temp->left = temp->right = NULL;
	return temp;
}

CNODE * makeHTree(CNODE * left, CNODE * right)
{
	CNODE *temp = (CNODE*)malloc(sizeof(CNODE));
	temp->weight = (left->weight)+(right->weight);
	temp->ch = (SIZE+1);
	temp->left = left;
	temp->right = right;
	temp->code[0] = 0;
	return temp;
}

void sortNodes(CNODE *nodeArr[])
{
	CNODE *tmp = NULL;
	int i, j;

	for (i = 1; i < CHAR_VARS; ++i)
	{
		j = i;
		tmp = nodeArr[i];
		while (j>0 && tmp->weight < (nodeArr[j - 1])->weight)
		{
			nodeArr[j] = nodeArr[j - 1];
			j--;
		}
		nodeArr[j] = tmp;
	}
}

CNODE * buildHTree(CHARC chars[])
{

	int i;
	CNODE *nodeArr[SIZE] = { NULL };

	//пустой узел
	CNODE empty;
	empty.weight = 99999;
	
	//создаем узлы и заносим в очередь
	for (i = 0; i < CHAR_VARS; ++i)
	{
		if (chars[i].count>1)
		nodeArr[i]=getNode(chars[i]);
		else nodeArr[i] = &empty;
	}

	//строим дерево
	//берем два последних узла и соединяем с новым
	//убираем узлы из очереди и заносим вместо них один вновьсозданый и один пустой
	//сортируем, повторяем пока не останется один не пустой

	//предварительная сортировка
	sortNodes(nodeArr);

	while ((nodeArr[1])->weight<100)
	{

		nodeArr[0] = makeHTree(nodeArr[0], nodeArr[1]);

		nodeArr[1]=&empty;

		//сортируем методом вставок
		sortNodes(nodeArr);

	}

	//возвращаем указатель на корень
	return nodeArr[0];
}