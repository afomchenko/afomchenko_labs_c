﻿/*
Huffman compressor
*/


#include "head.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define SIZE 256


int main(int argc, char * argv[])
{
	CHARC * chars;
	CNODE * root;
	CTABLE * cTable=NULL;
	FILE * fpIn;
	char head[6] = {0}; //заголовок
	int i;

	for (i = 1; i < argc; ++i)
	{

		//открываем коныертируемый файл
		fpIn = fopen(argv[i], "rb");
		if (fpIn == NULL) { perror(argv[i]); exit(1); }

		fread(&head, sizeof(char), 5, fpIn);
		fclose(fpIn);

		//смотрим на заголовок файла
		if (strcmp(head, "!huf!") == 0)
			//декомпрессия
		{
			printf("uncompress file %s...\n", argv[i]);

			if (uncompressFile(argv[i])) 			puts("OK!");
		}
		else
			//сжатие
		{
			printf("compress file %s...\n", argv[i]);
			puts("analyze...");

			chars = analyse(argv[i]);

			puts("build tree...");

			root = buildHTree(chars);

			puts("build code...");

			cTable = buildCodeTable(root, chars);

			puts("write code...");

			if (compressFile(cTable, argv[i], chars))	puts("OK!");
			free(chars);
			free(root);
			free(cTable);
		}


	}
	return 0;
}