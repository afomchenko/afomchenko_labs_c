﻿#define _CRT_SECURE_NO_WARNINGS

//таблица встечаемости
typedef struct
{
	int count;
	unsigned char ch;

} CHARC;

//таблица кодирования
typedef struct
{
	char code[24];
	unsigned char ch;
} CTABLE;


//байт вывода
typedef union
{
	unsigned char ch;
	struct BYTE
	{
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
	}byte;
} EXPCODE;

//узел дерева
struct NODE
{
	double weight;
	unsigned int ch;
	char code[24];
	struct NODE *left;
	struct NODE *right;
};
typedef struct NODE CNODE;

CHARC * analyse(const char * input);										//строим таблицу встечаемости
CNODE * buildHTree(CHARC chars[]);											//строим дерево
CTABLE * buildCodeTable(CNODE * root, CHARC * chars);						//строим таблицу кодов
int compressFile(CTABLE * cTable, const char * input, CHARC chars[]);		//жмем файл
int uncompressFile(const char * input);										//разжимаем сжатый файл