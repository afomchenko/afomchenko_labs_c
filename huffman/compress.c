﻿#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 256
#define CHAR_VARS 256
#define CODE_SIZE 24

extern int fsz;




void codeToFile(const char expBuf[], FILE * fpOut, const unsigned char tail)
{
	int i;
	EXPCODE byteWrite;
	byteWrite.ch = 0;

	for (i = 0; i < SIZE && i < SIZE-(tail); i+=8)
	{
		//заполняем битовое поле
		//48 - "0" в ascii

		byteWrite.byte.b1 = (unsigned)expBuf[i]-48;
		byteWrite.byte.b2 = (unsigned)expBuf[i + 1] - 48;
		byteWrite.byte.b3 = (unsigned)expBuf[i + 2] - 48;
		byteWrite.byte.b4 = (unsigned)expBuf[i + 3] - 48;
		byteWrite.byte.b5 = (unsigned)expBuf[i + 4] - 48;
		byteWrite.byte.b6 = (unsigned)expBuf[i + 5] - 48;
		byteWrite.byte.b7 = (unsigned)expBuf[i + 6] - 48;
		byteWrite.byte.b8 = (unsigned)expBuf[i + 7] - 48;

		
		/*printf("%d %d %d %d %d %d %d %d %#x\n",
			byteWrite.byte.b1,
			byteWrite.byte.b2,
			byteWrite.byte.b3,
			byteWrite.byte.b4,
			byteWrite.byte.b5,
			byteWrite.byte.b6,
			byteWrite.byte.b7,
			byteWrite.byte.b8,
			byteWrite.ch
			);*/
		
		fwrite(&byteWrite.ch, 1, 1, fpOut);
	}

}



int compressFile(CTABLE * cTable, const char * input, CHARC chars[])
{
	unsigned char c, tail=0;
	int i, codeSize=0;
	int bufSize = 0;
	char code[SIZE] = {0};
	FILE * fpIn;
	FILE * fpOut;
	char expBuf[SIZE + CODE_SIZE] = { 0 };
	char filename[SIZE], fileExt[16] = { 0 };
	char proc, procold = -1;

	//генрируем имя файла из входного

	for (i = strlen(input); i > 0; --i)
	{
		if (input[i] == '.')
		{
			strcpy(filename, input);
			strcpy(fileExt ,filename+i);
			strcpy(filename+i, ".huf");
			break;
		}

	}
	if (fileExt[0] == 0)
	{
		strcpy(filename, input);
		strcat(filename, ".huf");
	}



	fpIn = fopen(input, "rb");
	if (fpIn == NULL) { perror(input); exit(1); }

	fpOut = fopen(filename, "wb");


	//заголовок
	fwrite("!huf!", sizeof(char), 5, fpOut);
	//расширение
	fwrite(&fileExt, sizeof(char), 16, fpOut);
	//длина исходного файла
	fwrite(&fsz, sizeof(int), 1, fpOut);
	//таблица символов
	for (i = 0; i < CHAR_VARS; ++i)
	{
		fwrite(&chars[i], sizeof(CHARC), 1, fpOut);
	}
	//

	//записываем коды в файл
	while (ftell(fpIn)<fsz)
	{
		//счетчик прогресса
		proc = (char)(((float)ftell(fpIn) / fsz) * 100);
		if (proc == procold + 1) { printf("%c%c%c%c%d%c", 8, 8, 8, 8, proc, '%'); procold++; }


		//считываем в буфер
		while (	bufSize < SIZE && 
				fread(&c, sizeof(char), 1, fpIn))
		{		
			codeSize = strlen(cTable[c].code);
			bufSize = bufSize + codeSize;
			strcat(expBuf, cTable[c].code);
		}

		//если файл кончился
		if (feof(fpIn))
		{
			tail = SIZE - bufSize;
		}
		
		//пишем код в файл
		codeToFile(expBuf, fpOut, tail);

		//переносим в начало что считано но не влезло в буфер
		if (bufSize > SIZE)
		{
			strcpy(expBuf, expBuf + SIZE);
			bufSize = bufSize - SIZE;
		}
		//иначе просто стираем буфер
		else 
		{
			bufSize = 0;
			expBuf[0] = 0;
		}
		
	}

	printf("%c%c%c%c%d%c\n", 8, 8, 8, 8, 100, '%');
	fclose(fpIn);
	fclose(fpOut);
	return 1;
}