﻿#include "head.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define CHAR_VARS 256
#define CODE_SIZE 24


void buildCodeNode(CNODE * root)
{
	if (root)
	{
		if (root->left)
		{
			strcpy(root->left->code, root->code);
			strcat(root->left->code, "0");
			buildCodeNode(root->left);
		}
		if (root->right)
		{
			strcpy(root->right->code, root->code);
			strcat(root->right->code, "1");
			buildCodeNode(root->right);
		}
	}

}

void getCodeNode(char code[], CNODE *root, const unsigned char ch)
{
	if (root->ch == (unsigned int)ch)
	{
		strcpy(code, root->code);

	}
	if (root->left)
	{
		getCodeNode(code, root->left, ch);
	}
	if (root->right)
	{
		getCodeNode(code, root->right, ch);
	}


}


CTABLE * buildCodeTable(CNODE * root, CHARC * chars)
{

	CTABLE *cTable = (CTABLE*)malloc(SIZE*sizeof(CTABLE));
	int i, j;
	char code[CODE_SIZE];


	//генерируем коды
	buildCodeNode(root);


	//создаем таблицу кодов
	for (i = 0; i < CHAR_VARS; ++i)
	{
		//получаем код из дерева
		getCodeNode(code, root, (chars[i]).ch);

		//заносим в таблицу
		cTable[i].ch = (chars[i]).ch;
		strcpy(cTable[i].code, code);
	}

	//сортируем методом вставок
	CTABLE tmp;

	for (i = 1; i < CHAR_VARS; ++i)
	{
		j = i;
		tmp = cTable[i];
		while (j>0 && tmp.ch< cTable[j - 1].ch)
		{
			cTable[j] = cTable[j - 1];
			j--;
		}
		cTable[j] = tmp;
	}

	return cTable;
}