﻿#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 256
#define CHAR_VARS 256

extern int fsz;


static EXPCODE byteRead;
static int dirNodeCounter = 9;


void searchNode(CNODE *root, FILE * fpIn, FILE * fpOut)
{
	unsigned char bMask = 1;
	unsigned char dirct;

	//считываем очередной байт
	if (dirNodeCounter > 7)
	{
		fread(&byteRead.ch, 1, 1, fpIn);
		dirNodeCounter = 0;
	}

	//вычисляем направдение по дереву
	dirct = ((byteRead.ch & (bMask << dirNodeCounter))>0);
	
	//смещаемся по дереву
	if (root->ch <= SIZE)
		{
			fwrite(&root->ch, sizeof(char), 1, fpOut);
		}
	   else if (dirct!=0)
		{
		   dirNodeCounter++;
		   searchNode(root->right, fpIn, fpOut);
		}
	   else if (dirct == 0)
		{
		   dirNodeCounter++;
			searchNode(root->left, fpIn, fpOut);
		}
}

int uncompressFile(const char * input)
{
	CHARC *chars = (CHARC*)malloc(SIZE*sizeof(CHARC));
	CNODE * root;


	int i;
	FILE * fpIn;
	FILE * fpOut;
	char filename[SIZE];
	char fileExt[16];
	char proc, procold = -1;



	fpIn = fopen(input, "rb");
	if (fpIn == NULL) { perror(input); exit(1); }



	//смещаемся на длину заголовка
	fseek(fpIn, 5, SEEK_SET);

	// считываем расширение
	fread(&fileExt, sizeof(char), 16, fpIn);


	//считываем размер раскодированного файла
	fread(&fsz, sizeof(int), 1, fpIn);

	//считываем таблицу символов
	for (i = 0; i < CHAR_VARS; ++i)
	{
		fread(&chars[i], sizeof(CHARC), 1, fpIn);
	}

	//строим дерево
	puts("build code...");
	root=buildHTree(chars);

	//декодируем
	puts("decode...");



	//генрируем имя раскодированного файла
	strcpy(filename, input);
	strcpy(filename+strlen(filename)-4, ".uhf");
	strcat(filename, fileExt);

	fpOut = fopen(filename, "wb");

	for (i = 0; i < fsz; ++i)
	{
		//ищем символ по коду
		searchNode(root, fpIn, fpOut);

		//счетчик прогресса
		if (i % 1024==0){
		proc = (char)(((float)i / fsz) * 100);
		if (proc == procold + 1) { printf("%c%c%c%c%d%c", 8, 8, 8, 8, proc, '%'); procold++; }
			}
	}

	printf("%c%c%c%c%d%c\n", 8, 8, 8, 8, 100, '%');
	fclose(fpIn);
	fclose(fpOut);


	dirNodeCounter = 9;

	return 1;
}