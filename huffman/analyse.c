﻿#include "head.h"
#include <stdio.h>
#include <stdlib.h>
#define SIZE 256
#define CHAR_VARS 256

extern int fsz=0;



CHARC * analyse(const char * input)
{

	CHARC *chars = (CHARC*)malloc(SIZE*sizeof(CHARC));
	FILE *fp;
	unsigned char c;
	int proc ,fszproc, procent=0;
	int i, j;

	for (i = 0; i < SIZE; ++i)
	{
		chars[i].ch = i;
		chars[i].count = 1;
	}


	fp = fopen(input, "rb");
	if (fp == NULL) { perror(input); exit(1); }


	//длина файла
	fseek(fp, 0, SEEK_END);
	fsz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	proc= fszproc = fsz / 100;

	//составляем таблицу
	for (j = 0; j < fsz; ++j)
	{
		fread(&c, sizeof(char), 1, fp);
		chars[c].count++;


		//счетчик прогресса
		if (j == proc) { printf("%c%c%c%c%d%c", 8, 8, 8, 8, procent, '%'); proc += fszproc; procent++; }
	}

	printf("%c%c%c%c%d%c\n", 8, 8, 8, 8, 100, '%');
	//сортируем таблицу методом вставок
	CHARC tmp;
	for (i = 1; i < CHAR_VARS; ++i)
	{
		j = i;
		tmp = chars[i];
		while (j>0 && tmp.count < chars[j - 1].count)
		{
			chars[j] = chars[j - 1];
			j--;
		}
		chars[j] = tmp;
	}

	fclose(fp);

	return chars;
}