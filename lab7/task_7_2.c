/*
7.1
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define SIZE 256

typedef struct
{
	int count;
	unsigned char ch;
} SYM;





int main(int argc, char * argv[])
{

	SYM chars[SIZE];
	FILE *fp;
	unsigned char c;
	int i, j, chnum = 0, found = 0;
	fp = fopen(argv[1], "rt");
	if (fp == NULL) { perror(argv[1]); exit(1); }

	while (!feof(fp))
	{
		c = fgetc(fp);
		if (c!='\n' && c !=' ' && c!=255)
		{
			if (isupper(c)) c = tolower(c);
			for (i = 0; i < chnum; ++i)
			{
				if (chars[i].ch == c)
				{
					chars[i].count++;
					found = 1;
					break;
				}
			}
			if (found == 0){ chars[chnum].ch = c;  chars[chnum].count = 1; chnum++; }
			else found = 0;
		}
	}

	//��������� ������� �������
	SYM tmp;

	for (i = 1; i < chnum; ++i)
	{
		j = i;
		tmp = chars[i];
		while (j>0 && tmp.count > chars[j - 1].count)
		{
			chars[j] = chars[j - 1];
			j--;
		}
		chars[j] = tmp;
	}

	system("chcp 1251 -> NUL");
	for (i = 0; i < chnum; ++i)
	{
		printf("%c%d - %d\n", chars[i].ch, chars[i].ch, chars[i].count);
	}


	fcloseall();
	return 0;
}