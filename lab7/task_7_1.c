/*
7.1
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define SIZE 256

struct NODE
{
	int count;
	char word[SIZE];
	struct NODE *left;
	struct NODE *right;
};

typedef struct NODE WNODE;


WNODE * makeTree(WNODE *root, char *word)
{
	if (root == NULL)
	{
		WNODE *temp = (WNODE*)malloc(sizeof(WNODE));
		temp->count = 0;
		strcpy(temp->word, word);
		temp->left = temp->right = NULL;
		return temp;
	}
	else if (strcmp(root->word, word) > 0)
	{
		root->left = makeTree(root->left, word);
		return root;
	}
	else if (strcmp(root->word, word) < 0)
	{
		root->right = makeTree(root->right, word);
		return root;
	}

	return root;
}

int searchTree(WNODE *root, int *arr, int len)
{
	
	int i, comp=0;
   
	if (root)
	{


		if (len == strlen(root->word))
		{

			for (i = 0; i < len; i++)
			{
				if (arr[i] != root->word[i]){ comp = 1; break; }
			}
			if (comp != 1) { root->count++; return 1; }

		}	

		if (root->left)
			searchTree(root->left, arr, len);

		if (root->right)
			searchTree(root->right, arr, len);
	}
	return 0;
}

void printTree(WNODE *root)
{
	int comp = 0;
	if (root)
	{
		if (root->left)
			printTree(root->left);

		if (root->count > 0) printf("%s - %d\n", root->word, root->count);

		if (root->right)
			printTree(root->right);
	}
}

int main(int argc, char * argv[])
{
	WNODE *root = NULL;
	FILE *fp;
	char buf[SIZE]; int *arr;
	int i=0, j, k, fsz;

	fp = fopen("key.txt", "rt");
	if (fp == NULL) { perror("key.txt"); exit(1); }
	
	while (!feof(fp))
	{
		fscanf(fp, "%s", buf);
		root = makeTree(root, buf);
	}
	fclose(fp);

	fp = fopen("task_7_1.c", "rt");
	if (fp == NULL) { perror("task_7_1.c"); exit(1); }
	fseek(fp, 0, SEEK_END);
	fsz = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	arr = (int*)malloc(sizeof(int)*fsz);
	
	while (!feof(fp))
	{
		arr[i] = fgetc(fp);
		i++;
	}
	fclose(fp);
	
	for (j = 0; j < i; j++)
	{
		if (isalnum(arr[j]))
		{
			for (k = j; k < i; k++)
			{
				if (!isalnum(arr[k])) break;
			}
			searchTree(root, arr+j, k-j);
			j = j + (k - j)-1;
		}
	}

	printTree(root);

	return 0;
}