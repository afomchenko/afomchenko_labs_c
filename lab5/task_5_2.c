/*
5.2 �������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>
#define N 20
#define M 10

void arrPrint(char *p[]);
void arrRandFill(char *p[]);


int main()
{
	unsigned int i, j;
	char arr[M][N];
	char *p[M];

	for (i = 0; i < M; ++i) p[i] = arr [i];


	for (j = 0; j < 30; ++j)
	{		
		system("cls"); //������� �����
		arrRandFill(p); //��������� ����� ������� ��������
		arrPrint(p); //������� ������ � ���������� ���������
		Sleep(1000); //����� 1 �������
	}

	return 0;
}


void arrPrint(char *p[])
{
	putchar('\n');		putchar('\n');
	unsigned int i, j;

	//������� ��������
	for (i = 0; i < M; ++i){
		for (j = 0; j < 40 - N - 1; ++j)
			putchar(' '); //������
		for (j = 0; j < N; ++j)
			putchar(*(p[i] + j));
		for (j = N; j > 0; --j)
			putchar(*(p[i] + j-1));
		putchar('\n');
	}
	//������ ��������
	for (i = M; i > 0; i--){
		for (j = 0; j < 40 - N - 1; ++j)
			putchar(' '); //������
		for (j = 0; j < N; ++j)
			putchar(*(p[i-1] + j));
		for (j = N; j > 0; --j)
			putchar(*(p[i-1] + j-1));
		putchar('\n');
	}
}


void arrRandFill(char *p[])
{
	unsigned int i, j;
	srand((unsigned)time(NULL));
	for (i = 0; i < M;++i)
	for (j = 0; j < N; ++j)
	{
		*(p[i] + j) = rand() % 2 == 0 ? ' ' : '*'; 
	}
}