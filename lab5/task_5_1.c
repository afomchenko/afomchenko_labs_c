/*
5.1 �������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#define N 256
void chomp(char buf[]);
int printWord(char *p[], unsigned int j);
int getWords(char buf[], char *p[]);

int main()
{
	char buf[N];
	char *p[256];
	unsigned int i, j;
	puts("Enter string:   ");
	fgets(buf, N, stdin);
	chomp(buf); //������� \n � ����� ������

	j=getWords(buf, p);

	for (i = 0; i < 10; ++i)
	printWord(p, j); putchar('\n');

	return 0;
}

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int printWord(char *p[], unsigned int j)
{
	srand((unsigned)time(NULL));
	char *temp;
	unsigned int i, k = 0;
	for (i = 0; i < 100; ++i)
	{
		int q = rand() % (j - 1);
		temp = p[q]; p[q] = p[q + 1]; p[q + 1] = temp;

	}

	for (i = 0; i < j; ++i)
	{
		while (isalnum(*(p[i] + k))) putchar(*(p[i] + k)), k++;
		putchar(' ');
		k = 0;
	}

	return 1;
}

int getWords(char buf[], char *p[])
{
	unsigned int j = 0;
	for (unsigned int i = 0; i < strlen(buf); ++i)
	{

		if (!isalnum(buf[i]) && isalnum(buf[i + 1])) p[j] = &buf[i + 1], j++;
		else if (isalnum(buf[i]) && i == 0)  p[j] = &buf[i], j++;
	}
	return j;
}