/*
5.1 �������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#define N 256 //������ �������
void chomp(char buf[]);

int wordPart(char *p, char *pword[])
{
	unsigned int j, k=0;

		for (j = 0; j<strlen(p); ++j)
		{
			if (isalnum(*p) && j == 0 || (isalnum(*(p + j)) && j>0 && !isalnum(*(p + j - 1)))) //������ �����
				pword[k] = p + j, k++;
			if (isalnum(*(p + j)) && (!isalnum(*(p + j + 1)))) //����� �����
				pword[k] = p + j, k++;
		}
	
		return k;//������ ������� ������ ����
}

void wordShuffle(char *p, char *pword[], int k)
{
	int i, j=0;
	char temp=0;
	srand((unsigned)time(NULL));
	for (j = 0; j < k; j+=2)
	{

		for (i = 0; i < 100; ++i)
		{
			
			int q = rand() % 15+1; //����� ���������� ����� � �����
			//printf("%d ", q);
			if (pword[j] + q + 1 < pword[j + 1] && pword[j] + q > pword[j])
			{
				temp = *(pword[j] + q); *(pword[j] + q) = *(pword[j] + q + 1); *(pword[j] + q + 1) = temp;
			}
		}
	}
}


int main()
{
	char buf[N][N];
	char *p[N];
	FILE *fp;

	fp = fopen("in.txt", "rb");
	if (fp == NULL) { puts("File error!"); return 1; }


	unsigned int i, k, strings = 0;

	while (fgets(buf[strings], N, fp))
	{
		//chomp(buf[strings]); //������� \n � ����� ������
		p[strings] = &buf[strings][0];
		strings++;
	}

	fclose(fp);

	
		for (i = 0; i < strings; ++i)
		{
			char *pword[N];
			k = wordPart(p[i], pword);//�������� �� ������
			wordShuffle(p[i], pword, k);//������������� ������
		}
	
		//����� � ����
		fp = fopen("out.txt", "wt");
		for (i = 0; i < strings; i++)
		{
			fprintf(fp, "%s", p[i]);
			fprintf(stdout, "%s", p[i]);
		}

	fclose(fp);

	return 0;
}

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}
