/*
6.8�������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ������-
��� ������. ������������� ��������� 4-� �������� ��������. ��-
����� ���������� ������������ �������� ��������.
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,) ��������� ��-
��� ���� ��������, �� ���� ������� �������� �� ������ ����� 3, 8, � �����
���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9). ������������-
��, ��� ������ � ��������� ������ ���������, �� ���� ���������� ��������
����� ���������� �������� � ��� �� ���������� ��������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

typedef struct
{
	char part; //��� ������
	double num; //��� �����
} PARTF;


int scanFormula(char * p, PARTF * poper)
{
	unsigned int i, j = 0, brCount=0;
	short neg = 0;
	for (i = 0; i < strlen(p); ++i)
	{
		if ((*(p + i) == '-' && i == 0) || (*(p + i) == '-' && i != 0 && *(p + i - 1) == '(')) neg = 1; //���� ���� ������������� �����
		else if (neg != 1 && *(p + i) == '+' || *(p + i) == '-' || *(p + i) == '*' || *(p + i) == '^' || *(p + i) == '%' || *(p + i) == '/' || *(p + i) == '(' || *(p + i) == ')') 
		{
			(poper + j)->part = *(p + i); 
			j++; 
		}
		else if (isdigit(*(p + i)))
		{
			(poper + j)->num = (*(p + i) - 48) + ((poper + j)->num * 10); 
			(poper + j)->part = 'n'; 
		}
		if (isdigit(*(p + i)) && !isdigit(*(p + i + 1)))
		{ 
			if (neg == 1)(poper + j)->num *= -1; 
			j++; 
			neg = 0; 
		}
		if (*(p + i) == '(') brCount++; // ������� ������
	}

	if (brCount == 0)brCount++;
	return brCount;
}

double eval(PARTF * op, unsigned int len)
{
	double a = 0, b = 0, res;
	char c = 0;
	unsigned int i, j, k = 0;

	do
	{
		c = 0, j=0;

		//���� ���� ���������� � �������
		for (i = 0; i < len; ++i)
		{
			if ((op + i)->part == '^')
			{
				a = (op + (i - 1))->num;
				b = (op + (i + 1))->num;
				c = (op + i)->part;
				j = i;
				break; 
			}
		}

		//������ ���� ��������� � �������
		for (i = 0; i < len; ++i)
		{
			if ((op + i)->part == '*' || (op + i)->part == '/' || (op + i)->part == '%')
			{
				a = (op + (i - 1))->num;
				b = (op + (i + 1))->num;
				c = (op + i)->part;
				j = i;
				break; 
			}
		}

		//������ �������� � ��������� ����� ������ ���������
		for (i = 0; i < len; ++i)
		{
			if (c == 0 && (op + i)->part == '+' || (op + i)->part == '-')
			{
				a = (op + i - 1)->num;
				b = (op + i + 1)->num;
				c = (op + i)->part;
				j = i;
				break;
			}
		}

		//���������
		switch (c) 
		{
		case '^': res = pow(a,b); break;
		case '%': if (b == 0) { puts("division by zero!"); res = 0;  break; }
			res = (int)a%(int)b; break;
		case '+': res = a + b; break;
		case '-': res = a - b; break;
		case '*': res = a * b; break;
		case '/': if (b == 0) { puts("division by zero!"); res = 0;  break; }
			res = (double)a / b; break;
		default: break;
		}

		//�������� ����������� ���������� �������� � ���� ��������
		(op + (j - 1))->num = (op + (j + 1))->num = (op + j)->num = res;
		(op + j)->part = (op + (j+1))->part = (op + (j-1))->part = 'n';

	} while (c != 0);

	return res;
}

double bracketExtract(PARTF * poper)
{
	PARTF * op = 0;
	PARTF * cl = 0;
	unsigned int i;
    int len;
	double res;

for (i = 0; (poper+i)->part != 0; ++i)
{	

	if ((poper + i)->part == '(') //���� ������� ����������� ������
	{ op = poper + i; len = i;}

	else if ((poper + i)->part == ')') // � ���������� � ��� �����������
	{ cl = poper + i; len = i - len;  break; }

}


if (op == 0) //���� ������ ������ ���
{ 
	len = i - 1;
	op = poper; 
	return eval(op+1, len); 
}

res = eval(op + 1, len);
(op)->num = (cl)->num = res; //������� ������ ������
(op)->part = (cl)->part = 'n';

return res;
}



int main()
{
	double res=0;
	PARTF oper[256] = { 0 };
	char buf[256];
	char *p = buf;
	PARTF *poper = oper;
	printf("input expression:  ");
	fgets(buf, 256, stdin);

	int brCount = scanFormula(p, poper); //��������� ������ �� ����� � �����, ��������� ������ ��������
	
	for (int i = 0; i<brCount; ++i) //��������� ������� ��� ������� ������ ��� ���� ���� ������ ���
	res=bracketExtract(poper);

	printf("result: %.2lf\n", res);

	return 0;
}