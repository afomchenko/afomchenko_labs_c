﻿/*
6.2 Написать программу, которая находит в диапазоне целых чисел от 2 до 1000000 число,
формирующее самую длинную последовательность Коллатца

Последовательностью Коллатца называют числовой ряд, каждый элемент которого формируется 
в зависимости от чётности/ нечётности предыдущего по закону:
n→ 3n+ 1, если n нечётное.
n→ n/2, если n чётное.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

unsigned int collatz(unsigned int n)
{
	unsigned int j = 0;

		while (n != 1)
		{
			n = n % 2 ? (3 * n + 1):(n / 2 );
			j++;
		}

		return j;
}

int main()
{
	unsigned int i, j, max, maxj=0;
	for (i = 2; i <= 1000000; ++i)
	{
		j=collatz(i);
		if (maxj < j) maxj = j, max=i;
	}
		printf("max %d: %d steps\n", max, maxj);

	return 0;
}

