/*
6.5 �������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
���������:
��������� ���� �� ���������� ����� ������� � ����������� ������� � ��-
������� ������ ����������� ������� �� ����� ���� N
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>

void fibRec(unsigned long long int* f, unsigned long long int prev, unsigned  int i, unsigned  int n)
{
	if (i <= n)
	{
		unsigned long long int tmp = *f; //��������� �������
		*f += prev; //���������� ������� � ���������� � ���������� � �������
		fibRec(f, tmp, i + 1, n);
	}
}

unsigned long long int fib(unsigned int n) {
	unsigned long long int f = 1;
	fibRec(&f, 0, 2, n); //�������� ����������� �������
	return f;
}


int main()
{
	int n = 92,  i;
	unsigned long long int a = 1, b = 0, c;
	unsigned long long int fi[92][2];
	double fiTime[92][2], fiStartTime;

	//iterative
	fiStartTime = (double)clock() / CLOCKS_PER_SEC;
	for (i = 0; i < n; ++i)
	{
		c = a + b;
		fi[i][0] = c;
		fiTime[i][0] = (double)clock() / CLOCKS_PER_SEC;
		a = b, b = c;
	}

	//recursive
	fiStartTime = (double)clock() / CLOCKS_PER_SEC;
	for (i = 1; i <= n; ++i)
	{
		fi[i - 1][1] = fib(i);
		fiTime[i - 1][1] = (double)clock() / CLOCKS_PER_SEC;
	}
	printf("%18s | %18s \n", "iterative", "recursive");
	for (i = 0; i < n; ++i)
		printf("%20llu %5.3lf | %20llu %5.3lf \n", fi[i][0], fiTime[i][0], fi[i][1], fiTime[i][1]);

	//����� � ������� CSV (������ excel ����������� ��������������)
	FILE * fp;
	fp = fopen("fib_opt.csv", "wt");
	fprintf(fp, "%s;%s;%s;%s;%s\n", "number", "iterative", "it. time", "recursive", "rec.time");
	for (i = 0; i < n; ++i)
		fprintf(fp, "%d;%llu;%.3lf;%llu;%.3lf\n", i + 1, fi[i][0], fiTime[i][0], fi[i][1], fiTime[i][1]);
	fclose(fp);

	return 0;
}