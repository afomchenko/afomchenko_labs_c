/*
6.3 �������� ���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int chtoint(char buf[], int len)
{

	if (buf[0] != '-' && len >= 0) //��� ������������� �����
			return ((int)(buf[len] - 48)) + (10 * chtoint(buf, len - 1));

	else if (buf[0]=='-' && len > 0) //��� ������������� �����
		return ((int)(buf[len] - 48)*-1) + (10 * chtoint(buf, len - 1));

	else return 0;

}

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int intRangeTest(char buf[])
{
	if (buf[0] != '-' && buf[0] > '9' || buf[0] < '0')
		{puts("error! unknown format."); return 1;}
	else if (strlen(buf) > 9  && buf[0]!='-')
	{
		char maxint[] = "2147483647";
		for (unsigned int i = 0; i < strlen(buf); ++i)
		if (buf[i]>maxint[i]) {puts("error! number is out of int 32 bit range."); return 2;}
		else if (buf[i]<maxint[i]) break;
	}
	else if (strlen(buf) > 10 && buf[0] == '-')
	{
		char minint[] = "-2147483648";
		for (unsigned int i = 1; i < strlen(buf); ++i)
		if (buf[i]>minint[i]) { puts("error! number is out of int 32 bit range."); return 3; }
		else if (buf[i]<minint[i]) break;
	}
	return 0;
}

int main()
{
	char buf[12];

	//������ ������
	fgets(buf, 12, stdin);
	chomp(buf);

	if (intRangeTest(buf)) return 1; //�������� ������ �� ��������� ����� � INT �� ������ (-2147483648 �� 2147483647)

	int num=chtoint(buf, strlen(buf)-1); //������ ����� ����������� ������� �����������

	//�����
	printf("%d \n", num);
	return 0;
}

