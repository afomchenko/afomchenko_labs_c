/*
6.4 Написать программу, которая суммирует массив традиционным и
рекурсивным способами
Программа выполняет следующую последовательность действий:
(a) принимает из командной строки значение степени двойки M;
(b) находит размер динамического массива N = 2^M;
(c) выделяет память под динамический массив;
(d) случайным образом заполняет массив данными;
(e) находит сумму традиционным и рекурсивыным способом;
(f) сравнивает время выполнения суммирования трпдиционным и рекурсив-
ным способом;
(g) освобождает динамическую память
*/

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker, "/STACK:33554432") //увеличим размер стека
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <windows.h>

int * arrFill(int n)
{
	int * arr;
	arr = (int *) (calloc(n, sizeof(int)));
	srand((unsigned)time(NULL));
	for (int i = 0; i < n; ++i)
		*(arr + i) = rand() % 999 + 1;
	return arr;
}

int sumIterative(int * arr, int n)
{
	int sum=0;
	for (int i = 0; i < n; ++i)
	{
		sum += *(arr + i);
		//Sleep(1); //используется потому что рекурсия достигает конца стека быстрее чем происходит какое-либо значимое изменение таймера
	}
	return sum;
}


int sumRecursive(int * arr, int n)
{
	//Sleep(1);
	if (arr <= arr + n)
	{return *(arr + n) + *(arr) + sumRecursive(arr + 1, n - 2);}
	else return 0;
}

int main()
{
	int m, n;
	int sumI, sumR;
	double startTm, endTm, iTm, rTm;
	printf("input number:  "); //от 1 до 18
	scanf("%d", &m);

	//число элементов
	n = (int)pow(2, (double)m);

	//заполняем динамический массив
	int * arr = arrFill(n);

	//for (int i = 0; i < n; ++i)
		//printf("%d,", *(arr + i));
	puts("\n------------------------\n");

	//итеративный метод
	startTm = (double)clock()/CLOCKS_PER_SEC;
	sumI = sumIterative(arr, n);
	endTm = (double)clock() / CLOCKS_PER_SEC;

	printf("Summation array in %d numbers by iterative method: %d\n", n, sumI);
	printf("ended in %lf seconds\n", iTm=endTm-startTm);

	puts("\n------------------------\n");

	//рекурсивный метод
	startTm = (double)clock() / CLOCKS_PER_SEC;
	sumR = sumRecursive(arr, n-1);
	endTm = (double)clock() / CLOCKS_PER_SEC;

	printf("Summation array in %d numbers by recursive method: %d\n", n, sumR);
	printf("ended in %lf seconds\n", rTm=endTm-startTm);

	puts("------------------------\n");

	//сравнение времени
	if ((int)(rTm * 1000) > (int)(iTm * 1000)) printf("iterative summation faster in %.2lf times\n", rTm / iTm);
	else if ((int)(rTm * 1000) < (int)(iTm * 1000)) printf("recursive summation faster in %.2lf times\n", iTm / rTm);
	else if ((int)(rTm*1000) == (int)(iTm*1000)) printf("it's (almost) same time\n");

	free(arr);
	return 0;
}