/*
6.7 �������� ���������, ������� ������� ����� �� ���������
�������� ������� � ���� ���������� ����������� �������;
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>


void printMaze(char *p[])
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY); //���� ���������
	int i, j;
	for (i = 0; i < 29; ++i)
	{
		for (j = 0; j < 80; ++j)
		{
			if (*(p[i] + j) == '#' || *(p[i] + j) == 'X') putchar(*(p[i] + j));
			else putchar(' '); //�������� ���� ��������� �� ������
		}
	}
}

void moveConsole(unsigned int y, unsigned int x, char ch, WORD color)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coord = { x, y };
	SetConsoleCursorPosition(hOut, coord); //��������� ����� �������
	SetConsoleTextAttribute(hOut, color); //���� ������
	putchar(ch);
}

void moveX(char *p[], unsigned int *y, unsigned int *x, char *pos)
{
	srand((unsigned)time(NULL));

	//������� ������ �� ��������� ������� ������ � �� ����
	int dirMove[4][3] = { { *y, *x - 1, *(p[*y] + *x - 1) }, { *y - 1, *x, *(p[*y - 1] + *x) }, { *y, *x + 1, *(p[*y] + *x + 1) }, { *y + 1, *x, *(p[*y + 1] + *x) } };

	//������� ����������� ��� ������ �� ������� ����� ����������
	char dirMin = 127;
	for (int i = 0; i < 4; i++)
	if (dirMove[i][2] != '#' && dirMove[i][2] < dirMin) dirMin = dirMove[i][2];
    
	//�������� X
	int k;
	while (1)
	{
		k = rand() % 4;
		if (dirMove[k][2] == dirMin)
		{
			*(p[*y] + *x) = *pos + 1;
			moveConsole(*y, *x, *pos+1, FOREGROUND_RED);
			*y = dirMove[k][0]; *x = dirMove[k][1];
			*pos = dirMove[k][2];
			moveConsole(*y, *x, 'X', FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			break;
		}
	}

}



int main()
{
	FILE *fp;
	char maze[30][80];
	char *p[30];
	unsigned int y = 20, x = 42; //��������� �������
	unsigned int i, j, steps=0;
	char pos = 48; //0 � ascii, ��� ������ ��� ���������� �� ������

	fp = fopen("maze.txt", "rb");
	if (fp == NULL) { puts("File error!"); return 1; }

	for (i = 0; i < 30; ++i)
	{
		for (j = 0; j < 80; ++j)
		{

			maze[i][j] = fgetc(fp);

			//���������� ��������� �������
			if (maze[i][j] == '\r')
				maze[i][j] = fgetc(fp);
			if (maze[i][j] == '\n')
				maze[i][j] = fgetc(fp);

		}
		p[i] = &maze[i][0];
	}

	fclose(fp);
	printMaze(p);
	maze[y][x] = 'X';

	//����� ������
	while (1)
	{

		moveX(p, &y, &x, &pos); //����������� 
		Sleep(50);steps++;
		if (y == 0 || x == 0 || x == 79 || y == 28) break;
		
	}

	//����� ����� �����
	moveConsole(29, 0, ' ', 15);
	printf("Exit found in %d steps.   ", steps);

	return 0;
}

