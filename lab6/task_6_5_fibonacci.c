/*
6.5 �������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
���������:
��������� ���� �� ���������� ����� ������� � ����������� ������� � ��-
������� ������ ����������� ������� �� ����� ���� N
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>

long int fib(int n)
{
	if (n == 1 || n == 2) return 1;
	else return fib(n - 1) + fib(n - 2);
}

int main()
{
	int n=40, a = 1, b = 0, c, i;
	long int fi[40][2];
	double fiTime[40][2], fiStartTime;

	//iterative
	fiStartTime = (double)clock() / CLOCKS_PER_SEC;
	for (i = 0; i < n; ++i)
	{
		c = a + b;
		fi[i][0] = c;
		fiTime[i][0] = (double)clock() / CLOCKS_PER_SEC;
		a = b, b = c;
	}

	//recursive
	fiStartTime = (double)clock() / CLOCKS_PER_SEC;
	for (i = 1; i <= n; ++i) 
	{ 
		fi[i-1][1] = fib(i); 
		fiTime[i-1][1] = (double)clock() / CLOCKS_PER_SEC;
	}
		printf("%18s | %18s \n", "iterative", "recursive");
	for (i = 0; i < n; ++i)
		printf("%12d %5.3lf | %12d %5.3lf \n", fi[i][0], fiTime[i][0], fi[i][1], fiTime[i][1]);

	//����� � ������� CSV (������ excel ����������� ��������������)
	FILE * fp;
	fp = fopen("fib.csv", "wt");
	fprintf(fp, "%s;%s;%s;%s;%s\n", "number", "iterative", "it. time", "recursive", "rec.time");
	for (i = 0; i < n; ++i)
		fprintf(fp, "%d;%d;%.3lf;%d;%.3lf\n", i+1, fi[i][0], fiTime[i][0], fi[i][1], fiTime[i][1]);
	fclose(fp);

	return 0;
}