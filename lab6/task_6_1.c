/*
6.1 �������� ��������� ������� ��������� � ��������� ���������� ������� �� ��������� �*�
����������� ����������� � ������� ��� �� �������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define N 27 //������ �������

void boxClear(char *p[], int x, int y, int sz)
{
	int i, j;
	for (i = 0; i < sz; i++)
	for (j = 0; j < sz; j++)
		*(p[x+i] + y+j) = ' ';
}


void boxFract(char *p[], int x, int y, int sz)
{
	int i, j;
	for (i = 0; i < 3; i++)
	{

		for (j = 0; j < 3; j++)
		{
			if (i!=1 && j!=1)   boxClear(p, y+(sz / 3)*i, x+(sz / 3)*j, sz / 3); //������� ������� ��������
			else if (sz != 3) boxFract(p, y+(sz / 3)*i, x+(sz / 3)*j, sz / 3); //��������� ��� ���������
		}
	}
}



int main()
{
	unsigned int i, j;
	char arr[N][N];
	char *p[N];


	//��������� ������� ����������� � �������� ������� ����������
	for (i = 0; i < N; i++)
	{
	for (j = 0; j < N; j++)
	{
		arr[i][j] = '*';
	}
	p[i] = &arr[i][0];
    }

	//������������ �����������
	boxFract(p, 0, 0, N);


	//�����
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < 40-N; j++) putchar(' ');
		for (j = 0; j < N; j++)
		{
			putchar(' ');
			putchar(arr[i][j]);
		}
		putchar('\n');
	}


	return 0;
}

