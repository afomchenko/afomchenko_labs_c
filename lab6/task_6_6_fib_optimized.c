/*
6.6 �������� ���������� ����������� �������, ����������� n-��
������� ���� ���������, �� ��� ��������������� �������� �����-
���
���������:
����� ������� ��� �������: ���� ���������� ��������������� �� main � ��-
������ ������, ���������������, ������� � �������� �����������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void fibRec(unsigned long long int* f, unsigned long long int prev, unsigned  int i, unsigned  int n)
{
	if (i <= n)
	{
		unsigned long long int tmp = *f; //��������� �������
		*f += prev; //���������� ������� � ���������� � ���������� � �������
		fibRec(f, tmp, i + 1, n);
	}
}

unsigned long long int fib(unsigned int n) {
	unsigned long long int f = 1;
	fibRec(&f, 0, 2, n); //�������� ����������� �������
	return f;
}


int main()
{
	int n, j;
	printf("input number:");//�������� �� �������� 93
	scanf("%d", &j);

	//�����
	for (n = 1; n <= j; ++n)
		printf(" %3d %llu\n", n, fib(n));

	return 0;
}
