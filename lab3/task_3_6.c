/*
3.6 �������� ���������, ������������� ����� ����� � ������ ����� ���������� � ���������� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h> //rand
#include <time.h>
#define N 10 //���� ���������



int main()
{

	int num[N];
	int sum = 0;
	unsigned int i, start = 0, stop = 0;

	//��������� ������
	srand((unsigned)time(NULL));
	for (i = 0; i < N; i++)
	{
		num[i] = rand() % 100 + 1; //������ 1-100
		printf("%d\n", num[i]); //�����
	}

	//��������� ������� ��������
	for (i = 1; i < N; i++)
	{
		if (num[i] < num[start]) start = i;
		if (num[i] > num[stop]) stop = i;
	}

	//������ ������� ���� ����������
	if (stop < start) start += stop, stop = start - stop, start = start - stop;

	//����������
	for (i = start+1; i <= stop-1; i++) sum += num[i];

	//�����
	printf("--------\nsumma from %d to %d is %d\n", num[start],num[stop],sum);

	return 0;
}