/*
3.7 �������� ���������, ��������� ������� ������������� �������� �� ��������� ������.
������� ������������� �� �������� �������������.
� ������� ���������� ������ � ����� ��� ����������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{

	char buf[256];
	fgets(buf, 256, stdin);
	chomp(buf); //������� \n � ����� ������

	char ch[256] = { 0 }; int num[256] = { 1 }; //������� �������� � ���������
	int n = 0;
	unsigned int i, j; //�������� ��� ������
	//puts(buf);


	//������� �������
	for (i = 0; i < strlen(buf); i++)
	{
		n = 0;
		for (j = 0; j < strlen(ch); j++) {
			if (buf[i] == ch[j]){ num[j]++; n = 1; }
		}
		if (n != 1){
			ch[strlen(ch)] = buf[i]; num[strlen(ch)] = 1;
		}
	}



	//��������� ����������� �������
	char tmp;
	int tmpnum;

	for (i = 0; i < strlen(ch) - 1; i++){
		for (j = 0; j < strlen(ch) - 1; j++){
			if (num[j + 1] > num[j])
			{
				tmp = ch[j + 1]; tmpnum = num[j + 1];
				ch[j + 1] = ch[j]; num[j + 1] = num[j];
				ch[j] = tmp; num[j] = tmpnum;
			}
		}
	}

	puts("-----------"); //�����������


	//�����
	for (i = 0; i < strlen(ch); i++){
		printf(" %3c", ch[i]);
		printf(" %3d\n", num[i]);
	}



	return 0;
}