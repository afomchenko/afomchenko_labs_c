/*
3.4 �������� ���������, ������������� ����� ����� � ������
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{

	char buf[256], ch[2];
	fgets(buf, 256, stdin);
	chomp(buf); //������� \n � ����� ������
	unsigned int i, num=0, sum=0, len=1;
	unsigned const int maxlen = 3; //���� ����� ����� � ������

	for (i = 0; i <= strlen(buf); i++)
	{
		if (isdigit(buf[i]) && len <= maxlen)
		{
			ch[0] = buf[i];
			num = num * 10 + atoi(ch);
			len++;
		}
		else len = 1, sum += num, num = 0;
	}

	printf("summa- %d\n", sum);



	return 0;
}