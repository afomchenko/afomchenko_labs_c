/*
3.5 �������� ���������, ������������� ����� ����� � ������ ����� ������ ������������� � 
���������� ������������� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h> //rand
#include <time.h>
#define N 10 //���� ���������



int main()
{

	int num[N];
	int i, j = N - 1, sum = 0;
	unsigned int start = N, stop = N;

	//��������� ������
	srand((unsigned)time(NULL));
	for (i = 0; i < N; i++)
	{
		num[i] = rand() % 100 + 1; //������ 1-100
		num[i]= i % 2 == 1 ? -num[i] : num[i]; //������ ���� � ������� �������
		printf("%d\n", num[i]); //�����
	}

	//��������� ������� ��������
	for (i = 0; i < N; i++, j--)
	{
		if (num[i] < 0 && start == N) start = i;
		if (num[j] >= 0 && stop == N) stop = j;
	}

	//����������
	for (i = start; i <= stop; i++) sum += num[i];

	//�����
	printf("--------\nsumma %d\n", sum);

	return 0;
}