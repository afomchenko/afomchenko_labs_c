/*
3.9 �������� ���������, ��������� ���� ������������������ ���������� �������� �� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{

	char buf[256];
	char ch;
	unsigned int i, n=1, max=0;
	printf("input string:  ");
	fgets(buf, 256, stdin);
	chomp(buf); //������� \n � ����� ������

	for(i = 0; i < strlen(buf); i++)
	{
		while (buf[i] == buf[i + n]) n++;
		if (max < n) max = n, ch=buf[i] ;
		n = 1;
	}

	printf("%d ", max);
	for (i = 1; i <= max; i++) putchar(ch);
	putchar('\n');


	return 0;
}