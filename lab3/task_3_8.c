/*
3.8 �������� ���������, ��������� ����� �� ������ �� �������������� ������.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void chomp(char buf[])
{
	if (buf[strlen(buf) - 1] == '\n')	buf[strlen(buf) - 1] = 0;
}

int main()
{

	char buf[256];
	unsigned int i, word=0, n;
	printf("input string:  ");
	fgets(buf, 256, stdin);
	chomp(buf); //������� \n � ����� ������
	printf("input word number:  ");
	scanf_s("%d", &n);

	puts("-----------"); //�����������

	//������� ����� ����� n
	for (i = 0; i < strlen(buf); i++)
	{
		if (isalnum(buf[i + 1]) && !(isalnum(buf[i])) || isalnum(buf[i]) && i == 0) word++;
		if (word == n && isalnum(buf[i])) putchar(buf[i]);
	}

	//���� ������������� ����� �� ��������� ������
	if (n>word) puts("not enough words in string!");
	else putchar('\n');




	return 0;
}